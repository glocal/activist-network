<?php
/*
Plugin Name: Activist Network Sites
Description: Display the sites in your network, using template tag, shortcode or array. 
Author: Pea, Glocal
Author URI: http://glocal.coop
Version: 0.1
License: GPL
*/

/************* TODOs *****************/
// - Add @showjoin to allow for a Join promo link
// - Add @jointext to allow for custom text in Join promo link

/************* Parameters *****************/
// - @return - Return (display list of sites or return array of sites) (default: display)
// - @numbersites - Number of sites to display/return (default: no limit)
// - @excludesites - ID of sites to exclude (default: 1 (usually, the main site))
// - @sortby - newest, updated, active, alpha (registered, last_updated, post_count, blogname) (default: alpha)
// - @defaultimage - Default image to display if site doesn't have a custom header image (default: none)
// - @instanceid - ID name for site list instance (default: network-sites-RAND)
// - @classname - CSS class name(s) (default: network-sites-list)
// - @hidemeta - Select in order to update date and latest post. Only relevant when return = 'display'. (default: false)
// - @showjoin - Future
// - @jointext - Future
// - @hideimage - Select in order to hide site image. (default: false)

/************* Array Values *****************/
//array(5) {
//  ["site-1"] // where number is blog_id
//  array(10) {
//    ["blog_id"]
//    ["blogname"]
//    ["siteurl"]
//    ["path"]
//    ["registered"]
//    ["last_updated"]
//    ["post_count"]
//    ["custom-header"]
//    ["recent_post"]
//    array(7) {
//      ["post_id"]
//      ["post_author"]
//      ["post_slug"]
//      ["post_date"]
//      ["post_title"]
//      ["post_content"]
//      ["permalink"]
//      ["thumbnail"]
//	  }
//  }

// Add Widget
require_once dirname( __FILE__ ) . '/glocal-network-sites-widget.php';

// Add TinyMCE button
require_once dirname( __FILE__ ) . '/glocal-network-tinymce.php';


function glocal_networkwide_sites($parameters = []) {
    
    /** Default parameters **/
    $defaults = array(
        'return' => 'display',
        'numbersites' => 0,
        'excludesites' => '1', 
        'sortby' => 'alpha',
        'defaultimage' => null,
        'instanceid' => 'network-sites-' . rand(),
        'classname' => 'network-sites-list',
        'hidemeta' => false,
        'hideimage' => false,
    );
    
    // Parse & merge parameters with the defaults - http://codex.wordpress.org/Function_Reference/wp_parse_args
    $settings = wp_parse_args( $parameters, $defaults );
    
    // Strip out tags
    foreach($settings as $parameter => $value) {
        // Strip everything
        $settings[$parameter] = strip_tags($value);
    }
    
    // Extract each parameter as its own variable
    extract( $settings, EXTR_SKIP );
    
    // Hold value in new variable
    $exclude = $excludesites;
    // Strip out all characters except numbers and commas. This is working!
    $exclude = preg_replace("/[^0-9,]/", "", $exclude);
    // Convert string to array
    $exclude = explode(",", $exclude);
    
    // ** Get sites **
    $siteargs = array(
        'limit'      => $numbersites,
        'archived'   => 0,
        'spam'       => 0,
        'deleted'    => 0,
    );
    $sites = wp_get_sites($siteargs);
        
    /** Iterate through $sites **/
    foreach($sites as $site) {
        // If site id matches excludedsite parameter, skip it
        if( in_array( $site['blog_id'], $exclude ) ) {
			continue;
		}
        
        // Build array of sites and site info
        $site_id = $site['blog_id'];
        $sitedetails = get_blog_details($site_id);
                
        $site_list['site-' . $site_id] = array(
			'blog_id' => $site_id,  // Put site ID into array
			'blogname' => $sitedetails->blogname,  // Put site name into array
			'siteurl' => $sitedetails->siteurl,  // Put site URL into array
            'path' => $sitedetails->path,  // Put site path into array
            'registered' => $sitedetails->registered,
            'last_updated' => $sitedetails->last_updated,
            'post_count' => $sitedetails->post_count,
		);
        
        // Get site image
        // If the glocal_get_custom_image function exists
        if(function_exists('glocal_get_custom_image')) {
            $custom = glocal_get_custom_image($site_id);

            // If there is a WP custom-header, get that
            if($custom) {
                $site_list['site-' . $site_id]['custom-header'] = $custom;
            } elseif($defaultimage) {
                // If there is a default image pased in the parameter, get that
                $site_list['site-' . $site_id]['custom-header'] = $defaultimage;
            } else {
                // Otherwise, leave empty
                $site_list['site-' . $site_id]['custom-header'] = '';
            }
        // If the glocal_get_custom_image function doesn't exist    
        } else {
            // If there is a default image pased in the parameter, get that
            if($defaultimage) {
                $site_list['site-' . $site_id]['custom-header'] = $defaultimage;
            }
            // Otherwise, leave empty
            else {
                $site_list['site-' . $site_id]['custom-header'] = '';
            }
        }
                    
        // Switch to current blog
        switch_to_blog( $site_id );
        
        // Get most recent post
        $recent_posts = wp_get_recent_posts('numberposts=1');
        
        // Get most recent post info
        foreach($recent_posts as $post) {
            $post_id = $post['ID'];
            
            // Post into $site_list array
            $site_list['site-' . $site_id]['recent_post'] = array (
                'post_id' => $post_id,
                'post_author' => $post['post_author'],
                'post_slug' => $post['post_name'],
                'post_date' => $post['post_date'],
                'post_title' => $post['post_title'],
                'post_content' => $post['post_content'],
                'permalink' => get_permalink($post_id),
            );
            
            // If there is a featured image, add URL to array, else leave empty
            if( wp_get_attachment_url( get_post_thumbnail_id($post_id) ) ) {
                $site_list['site-' . $site_id]['recent_post']['thumbnail'] = wp_get_attachment_url(get_post_thumbnail_id($post_id));
            } else {
                $site_list['site-' . $site_id]['recent_post']['thumbnail'] = '';
            }
        }
        
        // Exit
        restore_current_blog();
        
    }
        
    /** Sorting **/
    // Convert parameter value to lowercase
    $sortby = strtolower($sortby);
    
    // If 'newest' sort by 'registered'
    if($sortby == 'newest') {
        usort($site_list, function ($b, $a) {
            return strcmp($a['registered'], $b['registered']);
        });
    }
    // If 'updated' sort by 'last_updated'
    elseif($sortby == 'updated') {
        usort($site_list, function ($b, $a) {
            return strcmp($a['last_updated'], $b['last_updated']);
        });
    }
    // If 'updated' sort by 'post_count'
    elseif($sortby == 'active') {
        usort($site_list, function ($b, $a) {
            return strcmp($a['post_count'], $b['post_count']);
        });
    }
    // Else sort by 'blogname'
    else {
        usort($site_list, function ($a, $b) {
            return strcmp($a['blogname'], $b['blogname']);
        });
    }

    /** Return sites **/
    // If return parameter is 'array', return array of sites
    if($return == 'array') {
        // array of sites
         return $site_list;
    }
    
    // If return parameter is anything but 'array', display a list of sites
    else {
        // display list of sites

        // Check class is valid - alphanumeric only, spaces, hyphen and dashes only
        $classname = preg_replace("/[^\w _-]/", "", $classname);
        
        $html = '<ul id="' . $instanceid . '" class="' . $classname .'">';
                
        // Loop through $site_list and generate mark-up to display
        foreach($site_list as $site) {
            
            $site_id = $site['blog_id'];
            
            $slug = str_replace('/','',$site['path']); // Strip slashes from path to get slug
            if(!$slug) { // If there is no slug (it's the main site), make slug 'main'
                $site_slug = 'main';
            }
            else { // Otherwise use the stripped path as slug  
                $site_slug = $slug;
            }
            
            $html .= '<li id="' . $site_id . '" data-posts="' . $site['post_count'] . '" data-slug="' . $site_slug . '" data-id="' . $site_id . '">' ;
            
                // If $hideimage parameter not true, display it
                if(!$hideimage) {
                    
                    $html .= '<div class="item-image thumbnail"><a href="' . $site['siteurl'] . '">';
                    
                     // If there is a custom site header, display it
                    if($site['custom-header']) {

                        $html .= '<img src="' . $site['custom-header'] . '" class="post-thumbnail item-image wp-post-image">';

                    }
                    
                    $html .= '</a></div>';

                }
            
                $html .= '<h3 class="post-title item-title"><a href="' . $site['siteurl'] . '">' . $site['blogname'] . '</a></h3>';

                // If $hidemeta parameter not true, display it
                if(!$hidemeta) {
                    
                    $html .= '<div class="meta item-modified byline"><span class="meta-heading modified-title">Last Updated</span> <time> ' . date_i18n( get_option( 'date_format' ), strtotime( $site['last_updated'] ) ) . '</time></div>';

                    // Recent Post
                    $html .= '<div class="meta"><span class="meta-heading">Latest Post</span> <a href="' . $site['recent_post']['permalink'] . '">' . $site['recent_post']['post_title'] . '</a></div>';
                    
                }
                        
            $html .= '</li>';        
            
        }
        
        $html .= '</ul>';
        echo $html;
        
    }
        
}


/************* Get Custom Headers *****************/

function glocal_get_custom_image($site_id) {
	//store the current blog_id being viewed
	global $blog_id;
	$current_blog_id = $blog_id;

	//switch to the main blog designated in $site_id
	switch_to_blog($site_id);

	$site_image = get_custom_header();

	//switch back to the current blog being viewed
	switch_to_blog($current_blog_id);

	return $site_image->thumbnail_url;
}


/************* Add Shortcode *****************/

/************* TODOs *****************/

/************* Sample Usage *****************/
// [network_sites numbersites="10" excludesites="1" sortby="active" defaultimage="http://activistnetwork.site/wp-content/uploads/2014/10/glocal-logo-gray.png" instanceid="site-list" classname="network-site-list2" hidemeta="true" hideimage="true"] 

function glocal_networkwide_sites_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(), $atts )
	);

    if(function_exists('glocal_networkwide_sites')) {
        glocal_networkwide_sites( $atts );
    }
}
add_shortcode( 'network_sites', 'glocal_networkwide_sites_shortcode' );


